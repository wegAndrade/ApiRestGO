package main

import (
	"log"

	db "gitlab.com/wegAndrade/api-go-rest/Database"
	routes "gitlab.com/wegAndrade/api-go-rest/Routes"
)

func main() {
	db.ConectaComBancoDeDados()
	log.Printf("Iniciando servidor na porta :80000")
	routes.HandleRequest()

}
